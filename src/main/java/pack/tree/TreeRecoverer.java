package pack.tree;

/**
 * Created by Asus on 01.05.2019.
 */
public interface TreeRecoverer {
    TreeNode buildTree(int[] rootLeftRightOrder, int[] leftRootRightOrder) ;

}
