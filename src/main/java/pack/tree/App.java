package pack.tree;

import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App {





    public static void main( String[] args )
    {
        int[] leftRootRighOrder = new int[]{4, 2, 5, 1, 6, 7, 3, 8};
        int[] rootLeftRightOrder = new int[]{1, 2, 4, 5, 3, 7, 6, 8};

        TreeRecoverer treeRecoverer = new SlowTreeRecoverer();
        TreeNode tree = treeRecoverer.buildTree(rootLeftRightOrder, leftRootRighOrder);



        List<Integer> rootLeftRightOutput = new ArrayList<Integer>();
        TreeNode.rootLeftRightOutput(rootLeftRightOutput, tree);

        List<Integer> leftRootRightOutput = new ArrayList<Integer>();
        TreeNode.leftRootRightOutput(leftRootRightOutput, tree);

        System.out.println("Prefix out ");

        for(Integer a : rootLeftRightOutput)
            System.out.println(a);

        System.out.println("Infix out ");

        for(Integer a : leftRootRightOutput)
            System.out.println(a);

    }
}
