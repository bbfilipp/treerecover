package pack.tree;

import java.util.ArrayList;
import java.util.List;

public class TreeNode {
    private int rootVal;
    private  TreeNode left;
    private TreeNode right;

    public TreeNode(int rootVal){
        this.rootVal = rootVal;
    }

    public int getRootVal() {
        return rootVal;
    }

    public void setRootVal(int rootVal) {
        this.rootVal = rootVal;
    }

    public TreeNode getLeft() {
        return left;
    }

    public void setLeft(TreeNode left) {
        this.left = left;
    }

    public TreeNode getRight() {
        return right;
    }

    public void setRight(TreeNode right) {
        this.right = right;
    }

    public static void rootLeftRightOutput(List<Integer> start, TreeNode node){
        start.add(node.rootVal);
        if (node.left != null)
            rootLeftRightOutput(start, node.left);
        if (node.right != null)
            rootLeftRightOutput(start, node.right);
    }

    public static void leftRootRightOutput(List<Integer> start, TreeNode node){
        if (node.left != null)
            leftRootRightOutput(start, node.left);
        start.add(node.rootVal);
        if (node.right != null)
            leftRootRightOutput(start, node.right);
    }
}
