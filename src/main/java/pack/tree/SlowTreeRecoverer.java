package pack.tree;

/**
 * Created by Asus on 01.05.2019.
 */
public class SlowTreeRecoverer implements TreeRecoverer{

    @Override
    public TreeNode buildTree(int[] rootLeftRightOrder, int[] leftRootRightOrder) {
        if (rootLeftRightOrder.length != leftRootRightOrder.length) throw new IllegalArgumentException("Bad input data. Lengths must be equal.");

        int rootLeftRightStart = 0;
        int rootLeftRightEnd = rootLeftRightOrder.length-1;
        int leftRootRightStart = 0;
        int leftRootRightEnd = leftRootRightOrder.length-1;

        return makeTree(rootLeftRightOrder, rootLeftRightStart, rootLeftRightEnd, leftRootRightOrder, leftRootRightStart, leftRootRightEnd);
    }

    private TreeNode makeTree(int[] rootLeftRightOrder, int rootLeftRightStart, int rootLeftRightEnd,
                                    int[] leftRootRightOrder, int leftRootRightStart, int leftRootRightEnd){
        if(rootLeftRightStart>rootLeftRightEnd||leftRootRightStart>leftRootRightEnd){
            return null;
        }

        int rootVal = rootLeftRightOrder[rootLeftRightStart];
        TreeNode root = new TreeNode(rootVal);

        int k=0;
        for(int i=0; i<leftRootRightOrder.length; i++){
            if(rootVal == leftRootRightOrder[i]){
                k=i;
                break;
            }
        }

        root.setLeft(makeTree(rootLeftRightOrder, rootLeftRightStart+1,
                rootLeftRightStart+(k-leftRootRightStart), leftRootRightOrder, leftRootRightStart, k-1));
        root.setRight(makeTree(rootLeftRightOrder, rootLeftRightStart+(k-leftRootRightStart)+1,
                rootLeftRightEnd, leftRootRightOrder, k+1 , leftRootRightEnd));

        return root;
    }
}
